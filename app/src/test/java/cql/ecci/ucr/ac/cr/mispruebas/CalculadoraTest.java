package cql.ecci.ucr.ac.cr.mispruebas;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculadoraTest {

    @Test
    public void suma() throws Exception {
        assertEquals("Suma incorrecta", 4, new Calculadora().suma(2, 2));
    }

    @Test
    public void resta() throws Exception {
        assertEquals("Resta incorrecta", 0, new Calculadora().resta(2, 2));
    }

    @Test
    public void multiplica() throws Exception {
        assertEquals("Multiplica incorrecta", 4, new Calculadora().multiplica(2, 2));
    }
}