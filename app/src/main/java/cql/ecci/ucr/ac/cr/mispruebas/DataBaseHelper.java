package cql.ecci.ucr.ac.cr.mispruebas;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DataBaseHelper extends SQLiteOpenHelper {

    // Cada vez que cambie el esquema de la base de datos DataBaseContract,
    // debemos incrementar la version de la base de datos
    public static final int DATABASE_VERSION = 1;

    // Nombre de la base de datos
    public static final String DATABASE_NAME = "MiPruebas.db";

    // constructor de la clase, el contexto tiene la informacion global sobre el ambiente de la app
    public DataBaseHelper(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    // implementamos el metodo para la creacion de la base de datos
    public void onCreate(SQLiteDatabase db) {

        // Crear la base de datos de la app
        db.execSQL(DataBaseContract.SQL_CREATE_PERSONA);

        // Crear datos de prueba
        insertarPersonas(db);
    }

    // implementamos el metodo para la actualizacion de la base de datos
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Administracion de actualizaciones
        db.execSQL(DataBaseContract.SQL_DELETE_PERSONA);
        onCreate(db);
    }

    // implementamos el metodo para volver a la version anterior de la base de datos
    public void onDowngrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        onUpgrade(db, oldVersion, newVersion);
    }

    private void insertarPersonas(SQLiteDatabase db) {

        // Instancia la clase y realiza la inserción de datos
        try {
            //
            Persona mPersona = new Persona(
                    "1001",
                    "Immanuel Kant");
            // inserta
            long newRowId = insertarPersona(db, mPersona);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // insertar en la base de datos
    private long insertarPersona(SQLiteDatabase db, Persona mPersona) {

        // Crear un mapa de valores donde las columnas son las llaves
        ContentValues values = new ContentValues();

        values.put(DataBaseContract.DataBaseEntry._ID, mPersona.getIdentificacion());
        values.put(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE, mPersona.getNombre());

        // Insertar la nueva fila
        return db.insert(DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, null, values);
    }

    protected Persona leerPersona(SQLiteDatabase db, String identificacion) {

        Persona mPersona = new Persona();

        // Define cuales columnas quiere solicitar // en este caso todas las de la clase
        String[] projection = {
                DataBaseContract.DataBaseEntry._ID,
                DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE
        };

        // Filtro para el WHERE
        String selection = DataBaseContract.DataBaseEntry._ID + " = ?";
        String[] selectionArgs = {identificacion};

        // Resultados en el cursor
        Cursor cursor = db.query(
                DataBaseContract.DataBaseEntry.TABLE_NAME_PERSONA, // tabla
                projection, // columnas
                selection, // where
                selectionArgs, // valores del where
                null, // agrupamiento
                null, // filtros por grupo
                null // orden
        );

        // recorrer los resultados y asignarlos a la clase // aca podria implementarse un ciclo si es necesario
        if(cursor.moveToFirst() && cursor.getCount() > 0) {

            mPersona.setIdentificacion(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry._ID)));
            mPersona.setNombre(cursor.getString(cursor.getColumnIndexOrThrow(DataBaseContract.DataBaseEntry.COLUMN_NAME_NOMBRE)));
        }
        return mPersona;
    }
}
