package cql.ecci.ucr.ac.cr.mispruebas;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;

public class DataBaseDataSource {

    private Context mContext;

    public DataBaseDataSource(Context context) {

        mContext = context;
    }

    public Persona leerPersona(String identificacion) {

        // usar la clase DataBaseHelper para realizar la operacion de leer
        DataBaseHelper dataBaseHelper = new DataBaseHelper(mContext);

        // Obtiene la base de datos en modo lectura
        SQLiteDatabase db = dataBaseHelper.getReadableDatabase();

        return dataBaseHelper.leerPersona(db, identificacion);
    }
}
