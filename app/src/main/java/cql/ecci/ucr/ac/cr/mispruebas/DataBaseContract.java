package cql.ecci.ucr.ac.cr.mispruebas;

import android.provider.BaseColumns;

public class DataBaseContract {

    // Para asegurar que no se instancie la clase hacemos el constructor privado
    private DataBaseContract() {}

    // Definimos una clase interna que define las tablas y columnas
    // Implementa la interfaz BaseColumns para heredar campos estandar del marco de Android _ID
    public static class DataBaseEntry implements BaseColumns {

        // Clase Persona
        public static final String TABLE_NAME_PERSONA = "Persona";

        // identificacion, Utilizamos DataBaseEntry._ID de BaseColumns
        //
        public static final String COLUMN_NAME_NOMBRE = "nombre";
    }

    // Construir las tablas de la base de datos
    private static final String TEXT_TYPE = " TEXT";
    private static final String COMMA_SEP = ",";

    // Creacion de tablas
    public static final String SQL_CREATE_PERSONA =
            "CREATE TABLE " + DataBaseEntry.TABLE_NAME_PERSONA + " (" +
                    DataBaseEntry._ID + TEXT_TYPE + " PRIMARY KEY" + COMMA_SEP +
                    DataBaseEntry.COLUMN_NAME_NOMBRE + TEXT_TYPE + " )";

    public static final String SQL_DELETE_PERSONA =
            "DROP TABLE IF EXISTS " + DataBaseEntry.TABLE_NAME_PERSONA;
}
