package cql.ecci.ucr.ac.cr.mispruebas;

import android.content.Context;

import androidx.test.InstrumentationRegistry;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import org.junit.Test;
import org.junit.runner.RunWith;

import static org.junit.Assert.assertEquals;

@RunWith(AndroidJUnit4.class)
public class ExampleInstrumentedTestDb {

    public static final String TEST_STRING_ID = "1001";
    public static final String TEST_STRING_NAME = "Immanuel Kant";

    private Context mContext;
    private DataBaseDataSource mDataBaseDataSource;

    @Test
    public void testDataBaseDataSourceLeerPersona() throws Exception {

        mContext = InstrumentationRegistry.getTargetContext();
        mDataBaseDataSource = new DataBaseDataSource(mContext);

        Persona mPersona = mDataBaseDataSource.leerPersona(TEST_STRING_ID);

        assertEquals(TEST_STRING_ID, mPersona.getIdentificacion());
        assertEquals(TEST_STRING_NAME, mPersona.getNombre());
    }
}